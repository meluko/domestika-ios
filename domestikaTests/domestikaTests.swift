//
//  domestikaTests.swift
//  domestikaTests
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import XCTest
@testable import domestika

class domestikaTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testExample() {
        _ = self.expectation(description: "Async HTTP request")
        
        DomestikaApiClient.login("meluko@gmail.com", "anitaleg") { (login)  in
            DomestikaApiClient.setToken(login.token);
            
            DomestikaApiClient.getAccessoryGroups() { (accessoryGroups)  in
                print ("nada")
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

}
