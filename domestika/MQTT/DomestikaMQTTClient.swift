//
//  Client.swift
//  domestika
//
//  Created by Melvin on 25/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation
import MQTTClient

typealias MQTTSubscriptionHandler = (Data) -> Void

class DomestikaMQTTClient: NSObject {
    var transport: MQTTCFSocketTransport?
    var session: MQTTSession?
    var subscriptions: [String:MQTTSubscriptionHandler] = [:]
    let qos = MQTTQosLevel.atLeastOnce;
    
    init(withUsername username:String, password:String, connectHandler: @escaping MQTTConnectHandler) {
        super.init()

        transport = MQTTCFSocketTransport();
        transport?.host = "digital-shelter.ddns.net"
        transport?.port = 1883
   
        session = MQTTSession()
        session?.transport = transport
        session?.userName = username
        session?.password = password
        session?.delegate = self
        
        MQTTLog.setLogLevel(DDLogLevel.error)
        
        session?.connect(connectHandler: connectHandler)
    }
    
    func subscribeToTopic(_ topic:String, handler:@escaping MQTTSubscriptionHandler) {
        subscriptions[topic] = handler
        session?.subscribe(toTopic: topic, at: .atLeastOnce) { (error, number) in
            print("🔶 MQTT: subscribed to \(topic)")
        }
    }
    
}

extension DomestikaMQTTClient: MQTTSessionDelegate {

    func connected(_ session: MQTTSession!) {
        print("🔶 MQTT: connected")

        
    }
    func connected(_ session: MQTTSession!, sessionPresent: Bool) {
        print("🔶 MQTT: connected. sessionPresent: \(sessionPresent)")
        //self.session = session
    }

    func publishData(_ data:Data, onTopic topic:String) {
        session?.publishData(data, onTopic: topic, retain: true, qos: qos, publishHandler: { (error) in
            print("🔶 MQTT: published to 'foo'")
        })
    }
    
    func connectionError(_ session: MQTTSession!, error: Error!) {
        print("🔶 MQTT: connectionError")
    }
    func connectionRefused(_ session: MQTTSession!, error: Error!) {
        print("🔶 MQTT: connectionRefused")
    }
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16) {
        print("🔶 MQTT: messageDelivered")
    }
    func protocolError(_ session: MQTTSession!, error: Error!) {
        print("🔶 MQTT: protocolError")
    }
    func connectionClosed(_ session: MQTTSession) {
        print("🔶 MQTT: connectionClosed")
    }
    func newMessage(_ session: MQTTSession!, data: Data!, onTopic topic: String!, qos: MQTTQosLevel, retained: Bool, mid: UInt32) {
        guard let messageHandler = subscriptions[topic] else {
            return
        }
        messageHandler(data)
    }
    func handleEvent(_ session: MQTTSession!, event eventCode: MQTTSessionEvent, error: Error!) {
        switch eventCode {
        case .connected:
            print("🔶 MQTT: handleEvent connected")
        case .connectionClosed:
            print("🔶 MQTT: handleEvent connectionClosed")
        case .connectionClosedByBroker:
            print("🔶 MQTT: handleEvent connectionClosedByBroker")
        case .connectionError:
            print("🔶 MQTT: handleEvent connectionError")
        case .connectionRefused:
            print("🔶 MQTT: handleEvent connectionRefused")
        case .protocolError:
            print("🔶 MQTT: handleEvent protocolError")
        default:
            print("🔶 MQTT: handleEvent")
        }

    }

}
