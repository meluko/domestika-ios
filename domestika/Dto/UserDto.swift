//
//  UserDto.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

struct UserDto: Decodable {
    let id: Int
    let userName: String
    let email: String
    let floorId: Int
    let roles: [RoleDto];
}
