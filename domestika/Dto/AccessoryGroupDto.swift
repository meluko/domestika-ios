//
//  AccessoryGroupDto.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

struct AccessoryGroupDto: Decodable {
    let id: Int
    let name: String
    let description: String
    var active: Bool
}

struct AccessoryGroupResponseDto: Decodable {
    let count: Int
    let rows: [AccessoryGroupDto]
}

struct AccessoryGroupStatusConfessionDto: Decodable {
    let active: Bool
}
