//
//  LoginDto.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation
 
struct LoginDto: Decodable {
    let success: Bool
    let token: String
    let user: UserDto
}
