//
//  RoleDto.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

struct RoleDto: Decodable {
    enum Role: String, Decodable {
        case ADMIN, USER
    }
    
    let name: Role
}
