//
//  DomestikaApiClient.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

class DomestikaApiClient: NSObject {
    
    static var token: String?
    
    static func login(_ username:String, _ password: String, completion : @escaping (LoginDto) -> Void) {
        let url = "https://digital-shelter.ddns.net/api/v1/login"
        let request = HTTPRequest.post(url: url)
        let loginDictionary = [
            "username":username,
            "password":password
        ]
        request.addBodyFromDictionary(dict: loginDictionary)
        
        HTTPClient().performRequest(request: request) { (response, bodyString, error)  in
            guard error == nil else {
                return
            }
            
            guard bodyString != nil else {
                return
            }
            
            let jsonData = bodyString!.data(using: .utf8)!
            let login: LoginDto = try! JSONDecoder().decode(LoginDto.self, from: jsonData)
  
            completion(login)
        }
    }
    
    static func authenticateRequest(request: HTTPRequest) -> HTTPRequest {
        request.setValue(token ?? "", forHTTPHeaderField: "authorization")
        return request
    }
    
    static func getAccessoryGroups( completion : @escaping ([AccessoryGroupDto]) -> Void) {
        let url = "https://digital-shelter.ddns.net/api/v1/accessory-groups"
        let request = authenticateRequest(request: HTTPRequest.get(url: url))
        HTTPClient().performRequest(request: request) { (response, bodyString, error)  in
            guard error == nil else {
                return
            }
            
            guard bodyString != nil else {
                return
            }
            
            let jsonData = bodyString!.data(using: .utf8)!
            let accessoryGroups: AccessoryGroupResponseDto = try! JSONDecoder().decode(AccessoryGroupResponseDto.self, from: jsonData)
            
            completion(accessoryGroups.rows)
        }
    }
    
    static func activate(accessoryGroup: AccessoryGroup) {
        let id = accessoryGroup.dto!.id
        let url = "https://digital-shelter.ddns.net/api/v1/accessory-groups/\(id)/~actions/activate"
        let request = authenticateRequest(request: HTTPRequest.put(url: url))
        
        HTTPClient().performRequest(request: request) { (response, bodyString, error)  in}
    }
    
    static func deactivate(accessoryGroup: AccessoryGroup) {
        let id = accessoryGroup.dto!.id
        let url = "https://digital-shelter.ddns.net/api/v1/accessory-groups/\(id)/~actions/deactivate"
        let request = authenticateRequest(request: HTTPRequest.put(url: url))
        HTTPClient().performRequest(request: request) { (response, bodyString, error)  in}
    }
    
    static func setToken(_ newToken: String) {
        token = newToken
    }
}
