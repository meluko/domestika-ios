//
//  AccessoryGroupsViewController.swift
//  domestika
//
//  Created by Melvin on 25/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation
import UIKit
import MQTTClient

protocol AccessoryGroupDelegate {
    func activate(forAccessoryGroup accessoryGroup:AccessoryGroup)
    func deactivate(forAccessoryGroup accessoryGroup:AccessoryGroup)
    func subscribeToConfession(accessoryGroup:AccessoryGroup, handler: @escaping MQTTSubscriptionHandler);
}

class AccessoryGroup: NSObject {
    var dto:AccessoryGroupDto?
    var delegate: AccessoryGroupDelegate

    init(withDto dto:AccessoryGroupDto, delegate: AccessoryGroupDelegate) {
        self.dto = dto
        self.delegate = delegate
    }
}

class AccessoryGroupsViewController: UICollectionViewController {
    
    var accessoryGroups: [AccessoryGroup] = []
    let edgeInsetSize:CGFloat = 16.0
    let interCellMargin:CGFloat = 8.0
    let colsPerRow = 3
    var mqttClient: DomestikaMQTTClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        mqttConnect()
        loadAccessoryGroups()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setupCollectionView () {
        
        let layout = UICollectionViewFlowLayout()
        let flow = collectionView.collectionViewLayout
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: (view.frame.width-40)/2, height: (view.frame.width - 40)/2) // item size
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) // here you can add space to 4 side of item
        
        let nib = UINib.init(nibName: "AccessoryGroupCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "AccessoryGroupCell")
    }
    
    func loadAccessoryGroups() {
        DomestikaApiClient.login("meluko@gmail.com", "anitaleg") { (login)  in
            DomestikaApiClient.setToken(login.token);
            DomestikaApiClient.getAccessoryGroups { (accessoryGroups) in
                self.accessoryGroups = accessoryGroups.map({ (dto) -> AccessoryGroup in
                    return AccessoryGroup.init(withDto: dto, delegate: self)
                })
                self.collectionView.reloadData()
            }
        }
    }
    
    func mqttConnect () {
        mqttClient = DomestikaMQTTClient.init(withUsername:"iosClient", password: "iosClient") { (error) in
            
        }
    }
   
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accessoryGroups.count;
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccessoryGroupCell", for: indexPath) as! AccessoryGroupCell
        
        cell.setup(accessoryGroup: accessoryGroups[indexPath.row])
        return cell
    }
}


extension AccessoryGroupsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let totalWith = collectionView.frame.width
        let marginCount = CGFloat(colsPerRow - 1)
        let availableWidth = totalWith - (marginCount * interCellMargin) - (2 * edgeInsetSize)
        let width = CGFloat(Int(availableWidth) / colsPerRow)
        
        return CGSize(width: width, height: width)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt index: NSInteger) -> UIEdgeInsets {
        return UIEdgeInsets(top: edgeInsetSize, left: edgeInsetSize, bottom: edgeInsetSize, right: edgeInsetSize)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt index: NSInteger) -> CGFloat {
        
        return interCellMargin
    }

}

extension AccessoryGroupsViewController: AccessoryGroupDelegate {
    func subscribeToConfession(accessoryGroup: AccessoryGroup, handler: @escaping MQTTSubscriptionHandler) {
        let topic = "accessory-groups/\(accessoryGroup.dto!.name)/status/confess"
        mqttClient?.subscribeToTopic(topic, handler: handler)
    }

    func activate(forAccessoryGroup accessoryGroup:AccessoryGroup) {
        DomestikaApiClient.activate(accessoryGroup: accessoryGroup)
    }
    
    func deactivate(forAccessoryGroup accessoryGroup: AccessoryGroup) {
        DomestikaApiClient.deactivate(accessoryGroup: accessoryGroup)
    }
}


