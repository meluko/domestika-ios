//
//  HTTPResponseError.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

enum HTTPResponseError: Error {
    case ResponseError(statusCode: Int, body: String?)
    case RuntimeError(error: Error)
    
    func getError() -> [String: Any] {
        switch self {
        case .ResponseError(let statusCode,let body):
            return [
                "statusCode": statusCode,
                "body": body ?? [:]
            ]
        case .RuntimeError(let error):
            return [
                "error": error
            ]
        default:
            return [:]
        }
    }
}
