//
//  HTTPRequest.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

class HTTPRequest: NSObject {
    var request: URLRequest?;
    
    init(withMethod method: String, url:String) {
        let url = URL(string: url)!
        
        request = URLRequest(url: url)
        request?.httpMethod = method
        request?.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
    }
    
    func addBodyFromData(data: Data) {
        guard request != nil else { return }
        request!.httpBody = data
    }
    
    func addBodyFromDictionary(dict: [String:Any]) {
        let data = JSONRequestEncoder.encode(dic: dict)
        addBodyFromData(data: data!)
    }
    
    func getRequest() -> URLRequest {
        return request!
    }
    
    static func post(url: String) -> HTTPRequest {
        return HTTPRequest.init(withMethod: "POST", url: url)
    }
    
    static func get(url: String) -> HTTPRequest {
        return HTTPRequest.init(withMethod: "GET", url: url)
    }
    
    static func put(url: String) -> HTTPRequest {
        return HTTPRequest.init(withMethod: "PUT", url: url)
    }
    
    func setValue(_ value: String, forHTTPHeaderField httpHeaderField: String ) {
        request?.setValue(value, forHTTPHeaderField: httpHeaderField)
    }
    
    override var description : String {
        return "\(request!.url!.absoluteString)"
    }

}
