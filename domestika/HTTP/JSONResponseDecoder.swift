//
//  JSONResponseDecoder.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

class JSONResponseDecoder: NSObject {
    static func decode(json: String) -> [String:Any]? {
        if let data = json.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil;
    }
}
