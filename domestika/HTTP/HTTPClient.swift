//
//  HTTPClient.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

class HTTPClient: NSObject, URLSessionDelegate {
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let method = challenge.protectionSpace.authenticationMethod
        let host = challenge.protectionSpace.host
        switch (method, host) {
        case (NSURLAuthenticationMethodServerTrust, "localhost"):
            let trust = challenge.protectionSpace.serverTrust!
            let credential = URLCredential(trust: trust)
            completionHandler(.useCredential, credential)
        default:
            completionHandler(.performDefaultHandling, nil)
        }
    }
    
    func performRequest(request: HTTPRequest, completion: @escaping (HTTPURLResponse?, String?, HTTPResponseError?) -> Void) {
        let configuration:URLSessionConfiguration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)

        let task = session.dataTask(with: request.getRequest()) { (data, response, error) in
            guard error == nil else {
                return completion(nil, nil, HTTPResponseError.RuntimeError(error: error!))
            }
            guard
                let bodyString = String(data: data!, encoding: .utf8),
                let httpResponse = response as? HTTPURLResponse else {
                    return completion(nil, nil, nil)
            }
            
            print("🔻 \(httpResponse.statusCode) \(bodyString)")

            guard httpResponse.statusCode < 400 else {
                let internalError = HTTPResponseError.ResponseError(statusCode:httpResponse.statusCode, body: bodyString)
                

                
                return completion(httpResponse, bodyString, internalError)
            }
            
            completion(httpResponse,bodyString, nil)
            
        }
        
        print("🔺 \(request)")
            
        task.resume();
    }
}
