//
//  JSONRequestEncoder.swift
//  domestika
//
//  Created by Melvin on 24/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation

class JSONRequestEncoder: NSObject {
    static func encode(dic: [String:Any]) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}

