//
//  AccessoryGroupCell.swift
//  domestika
//
//  Created by Melvin on 25/04/2020.
//  Copyright © 2020 meluko. All rights reserved.
//

import Foundation
import UIKit
import Macaw

class AccessoryGroupCell: UICollectionViewCell {
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var transparentView: UIView!
    @IBOutlet var iconView: SVGView!
    
    let cornerRadius:CGFloat = 10.0
    let activeBackgroundColor = UIColor(displayP3Red: 1.0, green:0.1, blue: 0.3, alpha: 1.0)
    let inactiveBackgroundColor = UIColor(displayP3Red: 1.0, green:0.1, blue: 0.3, alpha: 0.5)
    
    let activeTextColor = UIColor.white;
    let inactiveTextColor = UIColor(displayP3Red: 1.0, green:1.0, blue: 1.0, alpha: 0.5);
    var accessoryGroup: AccessoryGroup?
    func setup(accessoryGroup: AccessoryGroup) {
        self.accessoryGroup = accessoryGroup
        guard let dto = accessoryGroup.dto else {
            return
        }
        
        layer.cornerRadius = cornerRadius
        descriptionLabel.text = dto.description
        descriptionLabel.sizeToFit()
        descriptionLabel.numberOfLines = 0
        
        isUserInteractionEnabled = true
        let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(self.handleSingleTap))
        addGestureRecognizer(singleTap)
        iconView.backgroundColor = UIColor.clear

        updateAccessoryGroup(active: dto.active)
        //iconView.backgroundColor = UIColor.clear
        self.accessoryGroup?.delegate.subscribeToConfession(accessoryGroup: accessoryGroup, handler: { (data) in
            let status = try! JSONDecoder().decode(AccessoryGroupStatusConfessionDto.self, from: data)
            self.updateAccessoryGroup(active: status.active)
        })
    }
    
    func updateAccessoryGroup(active:Bool) {
        if(active) {
            updateAccessoryGroupActiveLayout()
        } else {
            updateAccessoryGroupInactiveLayout()
        }
    }
    
    func updateAccessoryGroupActiveLayout() {
        statusLabel.text = NSLocalizedString("Active", comment: "");
        statusLabel.textColor = activeTextColor
        descriptionLabel.textColor = activeTextColor
        transparentView.backgroundColor = activeBackgroundColor
        iconView.fileName = "ActiveLightbulb"
    }
    
    func updateAccessoryGroupInactiveLayout() {
        statusLabel.text = NSLocalizedString("Inactive", comment: "");
        statusLabel.textColor = inactiveTextColor
        descriptionLabel.textColor = inactiveTextColor
        transparentView.backgroundColor = inactiveBackgroundColor
        iconView.fileName = "InactiveLightbulb"
    }

    @objc func handleSingleTap(recognizer: UITapGestureRecognizer) {
        if (accessoryGroup?.dto?.active ?? true) {
            accessoryGroup?.delegate.deactivate(forAccessoryGroup: accessoryGroup!)
        } else {
            accessoryGroup?.delegate.activate(forAccessoryGroup: accessoryGroup!)
        }
    }

}
